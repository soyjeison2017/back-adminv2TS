import { Request, Response } from 'express'

import { sendInformation } from '../utils/handleInformation'
import { SearchService } from '../services/search.service'

const SearchController = {
  getAllTotal: async (req: Request, res: Response) => {
    const getAll = await SearchService.getAllTodo(req, res)
    await sendInformation({ dataRecived: getAll })
  },
  getDocumentosColeccion: async (req: Request, res: Response) => {
    const getAll = await SearchService.getDocumentosColeccion(req, res)
    await sendInformation({ dataRecived: getAll })
  }

}

export { SearchController }
