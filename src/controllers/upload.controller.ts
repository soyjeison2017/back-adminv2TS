import { Request, Response } from 'express'

import { sendInformation } from '../utils/handleInformation'
import { UploadService } from '../services/upload.service'

const UploadController = {
  uploadFile: async (req: Request, res: Response) => {
    const uploadFile = await UploadService.uploadFile(req, res)
    await sendInformation({ dataRecived: uploadFile })
  },
  getImage: async (req: Request, res: Response) => {
    const getImage = await UploadService.getImage(req, res)
    await sendInformation({ dataRecived: getImage })
  }
}

export { UploadController }
