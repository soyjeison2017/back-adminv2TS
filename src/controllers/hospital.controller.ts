import { Request, Response } from 'express'

import { HospitalService } from '../services/hospital.service'
import { sendInformation } from '../utils/handleInformation'

const HospitalController = {
  getAllHospitals: async (req: Request, res: Response) => {
    const getHospitals = await HospitalService.getAllHospitals(req, res)
    await sendInformation({ dataRecived: getHospitals })
  },
  createHospital: async (req: Request, res: Response) => {
    const createHospital = await HospitalService.createHospital(req, res)
    await sendInformation({ dataRecived: createHospital })
  },
  updateHospital: async (req: Request, res: Response) => {
    const updateHospital = await HospitalService.updateHospital(req, res)
    await sendInformation({ dataRecived: updateHospital })
  },
  deleteHospital: async (req: Request, res: Response) => {
    const deleteHospital = await HospitalService.deleteHospital(req, res)
    await sendInformation({ dataRecived: deleteHospital })
  }
}

export { HospitalController }
