import { Request, Response } from 'express'

import { MedicoService } from '../services/medico.service'
import { sendInformation } from '../utils/handleInformation'

const MedicoController = {
  getAllMedicos: async (req: Request, res: Response) => {
    const getMedicos = await MedicoService.getAllMedicos(req, res)
    await sendInformation({ dataRecived: getMedicos })
  },
  getOneMedico: async (req: Request, res: Response) => {
    const getMedicos = await MedicoService.getOneMedico(req, res)
    await sendInformation({ dataRecived: getMedicos })
  },
  createMedico: async (req: Request, res: Response) => {
    const createMedico = await MedicoService.createMedico(req, res)
    await sendInformation({ dataRecived: createMedico })
  },
  updateMedico: async (req: Request, res: Response) => {
    const updateMedico = await MedicoService.updateMedico(req, res)
    await sendInformation({ dataRecived: updateMedico })
  },
  deleteMedico: async (req: Request, res: Response) => {
    const deleteMedico = await MedicoService.deleteMedico(req, res)
    await sendInformation({ dataRecived: deleteMedico })
  }
}

export { MedicoController }
