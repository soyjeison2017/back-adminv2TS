import { Request, Response } from 'express'

import { UserService } from '../services/user.service'
import { sendInformation } from '../utils/handleInformation'

const UserController = {
  getAllUsers: async (req: Request, res: Response) => {
    const getUsers = await UserService.getAllUsers(req, res)
    await sendInformation({ dataRecived: getUsers })
  },
  createUser: async (req: Request, res: Response) => {
    const createUser = await UserService.createUser(req, res)
    await sendInformation({ dataRecived: createUser })
  },
  updateUser: async (req: Request, res: Response) => {
    const updateUser = await UserService.updateUser(req, res)
    await sendInformation({ dataRecived: updateUser })
  },
  deleteUser: async (req: Request, res: Response) => {
    const deleteUser = await UserService.deleteUser(req, res)
    await sendInformation({ dataRecived: deleteUser })
  }
}

export { UserController }
