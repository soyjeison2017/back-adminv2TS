import { Request, Response } from 'express'

import { sendInformation } from '../utils/handleInformation'
import { AuthService } from '../services/auth.service'

const AuthController = {
  login: async (req: Request, res: Response) => {
    const login = await AuthService.login(req, res)
    await sendInformation({ dataRecived: login })
  },
  Logingoogle: async (req: Request, res: Response) => {
    const Logingoogle = await AuthService.Logingoogle(req, res)
    await sendInformation({ dataRecived: Logingoogle })
  },
  renewToken: async (req: Request, res: Response) => {
    const renewToken = await AuthService.renewToken(req, res)
    await sendInformation({ dataRecived: renewToken })
  }
}

export { AuthController }
