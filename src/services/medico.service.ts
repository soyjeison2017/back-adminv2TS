import { Request, Response } from 'express'
import { IResponseData } from '../interfaces/ResponseData'
import Medico from '../models/medico.model'

const MedicoService = {
  getAllMedicos: async (_req: Request, res: Response): Promise<IResponseData> => {
    try {
      const medicos = await Medico.find().populate('usuario', 'nombre img').populate('hospital', 'nombre img')
      return { ok: true, data: medicos, status: 200, res }
    } catch (error) {
      return { ok: true, data: 'Error al obtener los Medicos', status: 500, res, error }
    }
  },
  getOneMedico: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { id } = req.params
      const medico = await Medico.findById(id).populate('usuario', 'nombre img').populate('hospital', 'nombre img')
      return { ok: true, data: medico, status: 200, res }
    } catch (error) {
      return { ok: true, data: 'Error al obtener el Medico', status: 500, res, error }
    }
  },
  createMedico: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { body } = req

      const idUser = (req as any)._id as string | number
      const data = { usuario: idUser, ...body }
      const medicoCreate = await Medico.create(data)

      return { ok: true, data: { medico: medicoCreate }, status: 200, res }
    } catch (error) {
      return { ok: true, data: 'Error en la creación de Medico', status: 500, res, error }
    }
  },
  updateMedico: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { params: { id } } = req
      const usuarioid = (req as any)._id

      const medicoDB = await Medico.findById(id)

      if (medicoDB == null) {
        return { ok: false, data: 'Médico no encontrado', status: 404, res }
      }

      const cambiosMedico = {
        ...req.body,
        usuario: usuarioid

      }

      const medicoActualizado = await Medico.findByIdAndUpdate(id, cambiosMedico, { new: true })

      return { ok: true, data: { medico: medicoActualizado }, status: 200, res }
    } catch (error) {
      return { ok: true, data: { msg: 'Error al actualizar el medico' }, status: 500, res, error }
    }
  },
  deleteMedico: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { params: { id } } = req

      const medicoDB = await Medico.findById(id)

      if (medicoDB == null) {
        return { ok: false, data: 'Médico no encontrado', status: 404, res }
      }

      await Medico.findByIdAndDelete(id)
      return { ok: true, data: { msg: 'Medico Eliminado' }, status: 200, res }
    } catch (error) {
      return { ok: true, data: { msg: 'Error al eliminar el medico' }, status: 500, res, error }
    }
  }
}

export { MedicoService }
