import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'

import Usuario from '../models/user.model'
import { IResponseData } from '../interfaces/ResponseData'
import { generateJWT } from '../utils/generateJWT'
const { genSaltSync, hashSync } = bcrypt

const UserService = {
  getAllUsers: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const desde: number = Number((req.query as any).desde ?? 0)

      const [usuarios, totalUsuarios] = await Promise.all([
        Usuario.find({}, 'nombre email role google img')
          .skip(desde).limit(5),
        Usuario.countDocuments()
      ])

      return { ok: true, data: { usuarios, totalUsuarios }, status: 200, res }
    } catch (error) {
      return { ok: false, data: { msg: 'Error a obtener Usuarios' }, status: 500, res, error }
    }
  },
  createUser: async (req: Request, res: Response): Promise<IResponseData> => {
    const { body } = req
    try {
      const existEmail = await Usuario.findOne({ email: body.email })
      if (existEmail != null) return { ok: false, data: { msg: 'El Correo Electrónico ya existe' }, status: 400, res }

      const salt = genSaltSync()
      const passwordEncrypted = hashSync(body.password, salt)
      const user = await Usuario.create({ nombre: body.nombre, password: passwordEncrypted, email: body.email })
      user.set('password', undefined, { strict: false })

      const token = await generateJWT(user)

      const data = {
        user,
        token
      }

      return { ok: true, data, status: 200, res }
    } catch (error) {
      return { ok: false, data: { msg: 'Error en su creación' }, status: 500, res, error }
    }
  },
  updateUser: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const uid = req.params.id

      const userDb = await Usuario.findById(uid)
      if (userDb == null) return { ok: false, data: { msg: 'No existe usuario por ese ID' }, status: 404, res }

      const { password, email, google, ...campos } = req.body

      if (userDb.email !== email) {
        const existeEmail = await Usuario.findOne({ email })

        if (existeEmail != null) {
          return { ok: false, data: { msg: 'Este correo ya existe!' }, res, status: 403 }
        }
      }

      if (!userDb.google) {
        campos.email = email
      } else if (userDb.email !== email) {
        return { ok: false, data: { msg: 'Usuarios de google no se puede cambiar el correo electrónico' }, status: 400, res }
      }

      await Usuario.findByIdAndUpdate(uid, campos, { new: true })

      return { ok: true, data: campos, status: 200, res }
    } catch (error) {
      return { ok: false, data: { msg: 'Error en la actualización' }, status: 500, res, error }
    }
  },
  deleteUser: async (req: Request, res: Response): Promise<IResponseData> => {
    const uid = req.params.id

    try {
      const userDb = await Usuario.findById(uid)
      if (userDb == null) return { ok: false, data: { msg: 'No existe usuario por ese ID' }, status: 404, res }

      await Usuario.findByIdAndDelete(uid)
      return { ok: true, data: { msg: 'Usuario Eliminado' }, status: 200, res }
    } catch (error) {
      return { ok: false, data: { msg: 'Error en la ELiminación' }, status: 500, res, error }
    }
  }
}

export { UserService }
