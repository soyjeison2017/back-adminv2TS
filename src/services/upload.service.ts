import { Request, Response } from 'express'
import { IResponseData } from '../interfaces/ResponseData'
import { v4 as uuidv4 } from 'uuid'
import path from 'path'
import fs from 'fs'
import { actualizarImagen } from '../utils/updateImagen'

const MEDIA_PATH = path.join(__dirname, '../uploads')

interface Iimg {
  name: string
  data: any
  size: number
  encoding: string
  tempFilePath: string
  truncated: boolean
  mimetype: string
  md5: string
  mv: any
}

const UploadService = {
  uploadFile: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const tipo = req.params.tipo
      const id = req.params.id

      const types: string[] = ['hospitales', 'medicos', 'usuarios']

      if (!types.includes(tipo)) {
        return { ok: false, data: { msg: 'El tipo no existe' }, status: 400, res }
      }
      if ((req.files == null || req.files === undefined) || Object.keys(req.files).length === 0) {
        return { ok: false, data: { msg: 'No hay ningún archivo' }, status: 400, res }
      }

      const file: Iimg = req.files?.imagen as Iimg
      const nombreCortado = file.name.split('.')
      const extension = nombreCortado[nombreCortado.length - 1]
      const validarExtension = ['png', 'jpg', 'jpeg', 'gif']

      if (!validarExtension.includes(extension)) {
        return { ok: false, data: { msg: 'No es una extensión permitida' }, status: 400, res }
      }

      const nombreArchivo = `${uuidv4()}.${extension}`
      const filePath = `${MEDIA_PATH}/${tipo}/${nombreArchivo}`

      // Utiliza una promesa para manejar el proceso de mover la imagen
      await new Promise((resolve, reject) => {
        file.mv(filePath, (err: Error) => {
          if (err === null) {
            reject(err)
          } else {
            resolve(true)
          }
        })
      })

      console.log('entro acaaaaaaaaaa actualizar')
      await actualizarImagen(tipo, id, nombreArchivo)

      return { ok: true, data: { msg: 'Archivo subido', nombre: nombreArchivo }, status: 200, res }
    } catch (error) {
      return { ok: false, data: { msg: 'Error en la búsqueda' }, status: 500, res, error }
    }
  },
  getImage: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const type = req.params.tipo
      const photo = req.params.foto

      const pathImgen: string = path.join(__dirname, `../uploads/${type}/${photo}`)

      if (fs.existsSync(pathImgen)) {
        return { ok: true, data: pathImgen, status: 200, res, isFile: true }
      } else {
        const pathNoImgen = path.join(__dirname, '../uploads/noImg.jpg')

        return { ok: true, data: pathNoImgen, status: 200, res, isFile: true }
      }
    } catch (error) {
      return { ok: true, data: { msg: 'Ocurrio un error en la IMG' }, status: 500, res, error }
    }
  }
}

export { UploadService }
