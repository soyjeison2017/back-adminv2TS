import { Request, Response } from 'express'
import { IResponseData } from '../interfaces/ResponseData'
import Hospital from '../models/hospital.model'

const HospitalService = {
  getAllHospitals: async (_req: Request, res: Response): Promise<IResponseData> => {
    try {
      const hospitales = await Hospital.find().populate('usuario', 'nombre img')

      return { ok: true, data: hospitales, status: 200, res }
    } catch (error) {
      return { ok: false, data: 'Error al obtener los hospitales', status: 500, res, error }
    }
  },
  createHospital: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { body } = req
      const idUser = (req as any)._id as string | number

      const hospital = await Hospital.create({ usuario: idUser, ...body })
      return { ok: true, data: { hospital }, status: 200, res }
    } catch (error) {
      return { ok: false, data: { msg: 'Error en la creación' }, status: 500, res, error }
    }
  },
  updateHospital: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { params: { id } } = req
      const usuarioid = (req as any)._id

      const hospitalDB = await Hospital.findById(id)

      if (hospitalDB == null) {
        return { ok: false, data: 'Hospital no encontrado', status: 404, res }
      }

      const cambiosHospitales = {
        ...req.body,
        usuario: usuarioid
      }
      const hospitalActualizado = await Hospital.findByIdAndUpdate(id, cambiosHospitales, { new: true })
      return { ok: true, data: { hospital: hospitalActualizado }, status: 200, res }
    } catch (error) {
      return { ok: false, data: 'Error al actualizar el hospital', status: 500, res, error }
    }
  },
  deleteHospital: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { params: { id } } = req

      const hospitalDB = await Hospital.findById(id)

      if (hospitalDB == null) {
        return { ok: false, data: 'Hospital no encontrado', status: 404, res }
      }

      await Hospital.findByIdAndDelete(id)

      return { ok: true, data: { hospital: { msg: 'Hospital Eliminado' } }, status: 200, res }
    } catch (error) {
      return { ok: false, data: 'Error al eliminar el hospital', status: 500, res, error }
    }
  }
}

export { HospitalService }
