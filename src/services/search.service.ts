import { Request, Response } from 'express'

import Usuario from '../models/user.model'
import Hospital from '../models/hospital.model'
import Medico from '../models/medico.model'

import { IResponseData } from '../interfaces/ResponseData'

const SearchService = {
  getAllTodo: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const busqueda = req.params.searchtodo

      const regex = new RegExp(busqueda, 'i')

      const usuarios = await Usuario.find({ nombre: regex })
      const hospitales = await Hospital.find({ nombre: regex })
      const medicos = await Medico.find({ nombre: regex })

      return { ok: true, data: { usuarios, medicos, hospitales }, status: 200, res }
    } catch (error) {
      return { ok: false, data: { msg: 'Error en la búsqueda' }, status: 500, res, error }
    }
  },
  getDocumentosColeccion: async (req: Request, res: Response): Promise<IResponseData> => {
    const tabla = req.params.tabla
    const busqueda = req.params.searchtodo

    const regex = new RegExp(busqueda, 'i')
    let data = [] as any

    switch (tabla) {
      case 'medicos':
        data = await Medico.find({ nombre: regex })
          .populate('usuario', 'nombre img')
          .populate('hospital', 'nombre img')
        break
      case 'hospitales':
        data = await Hospital.find({ nombre: regex })
          .populate('usuario', 'nombre img')
        break
      case 'usuarios':
        data = await Usuario.find({ nombre: regex })
        break
      default:
        return { ok: true, data: { msg: 'Debe ser medicos, usuarios, hospitales' }, status: 400, res }
    }

    return { ok: true, data, status: 200, res }
  }

}

export { SearchService }
