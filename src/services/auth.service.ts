import { Request, Response } from 'express'
import { IResponseData } from '../interfaces/ResponseData'
import bcrypt from 'bcryptjs'

import Usuario from '../models/user.model'
import { generateJWT } from '../utils/generateJWT'
import { googleVerify } from '../utils/googleVerify'
import { menuExportFront } from '../utils/menuFront'

const AuthService = {
  login: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { email, password } = req.body

      const userDB = await Usuario.findOne({ email })
      if (userDB == null) return { data: { msg: 'Email o Contraseña no Valida' }, ok: false, status: 404, res }

      const validPassoword = bcrypt.compareSync(password, userDB.password)
      if (!validPassoword) return { data: { msg: 'Email o Contraseña no Valida' }, ok: false, status: 404, res }

      const token = await generateJWT(userDB)

      return { data: { token, menu: menuExportFront(userDB.role) }, status: 200, ok: true, res }
    } catch (error) {
      return { data: { msg: 'Ocurrió un error' }, status: 500, ok: false, res, error }
    }
  },
  Logingoogle: async (req: Request, res: Response): Promise<IResponseData> => {
    try {
      const { email, name, picture } = await googleVerify(req.body.token)

      const userDB = await Usuario.findOne({ email })
      let user

      if (userDB == null) {
        user = new Usuario({ nombre: name, password: '@@', email, img: picture, google: true })
      } else {
        user = userDB
        user.google = true
      }

      await user.save()
      const token = await generateJWT(userDB)

      return { data: { email, name, picture, token, menu: menuExportFront(user.role) }, status: 200, ok: true, res }
    } catch (error) {
      return { data: { msg: 'Ocurrió un error, Token de google' }, status: 500, ok: false, res, error }
    }
  },
  renewToken: async (req: Request, res: Response) => {
    const user = {
      id: (req as any)._id
    }

    const token = await generateJWT(user)

    const userDB = await Usuario.findById(user.id).lean() as any
    console.log(userDB)

    const { password, ...userLogged } = userDB

    return { data: { token, user: userLogged, menu: menuExportFront(userDB.role) }, status: 200, ok: true, res }
  }

}

export { AuthService }
