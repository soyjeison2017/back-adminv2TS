import { model, Schema } from 'mongoose'

const MedicoSchema = new Schema({
  nombre: {
    type: 'string',
    required: true
  },
  img: {
    type: String
  },
  usuario: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  },
  hospital: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'Hospital'
  }

})

export = model('Medico', MedicoSchema)
