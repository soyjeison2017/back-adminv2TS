import { model, Schema } from 'mongoose'

const HospitalSchema = new Schema({
  nombre: {
    type: 'string',
    required: true
  },
  img: {
    type: String
  },
  usuario: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  }

})

export = model('Hospital', HospitalSchema)
