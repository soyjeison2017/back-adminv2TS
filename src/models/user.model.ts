import { model, Schema } from 'mongoose'
import { Roles } from '../enums/user.enum'

const UserSchema = new Schema({
  nombre: {
    type: 'string',
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  img: {
    type: String
  },
  role: {
    type: String,
    required: true,
    default: Roles.user
  },
  google: {
    type: Boolean,
    default: false
  }
})

export = model('Usuario', UserSchema)
