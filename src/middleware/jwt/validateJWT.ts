import { NextFunction, Request, Response } from 'express'
import * as JWT from 'jsonwebtoken'
import { IResponseData } from '../../interfaces/ResponseData'
import { sendInformation } from '../../utils/handleInformation'
import { IJWT } from '../../interfaces/JWT.interface'
import Usuario from '../../models/user.model'

interface CustomRequest extends Request {
  _id?: string | number
}

const validateJWToken = async (req: CustomRequest, res: Response, next: NextFunction): Promise<any> => {
  const token = req.header('x-token')

  if (token == null) {
    const responseData: IResponseData = { data: { msg: 'Acceso denegado' }, status: 401, ok: false, res }
    return await sendInformation({ dataRecived: responseData })
  }

  try {
    const dataToken: IJWT = await JWT.verify(token, process.env.JWT_TOKEN as string) as any
    const uid = dataToken._id
    req._id = uid
    return next()
  } catch (error) {
    const responseData: any = { data: { msg: 'Token no Valido' }, status: 500, error, ok: false, res }
    return await sendInformation({ dataRecived: responseData })
  }
}

const validarAdminRole = async (req: CustomRequest, res: Response, next: NextFunction): Promise<any> => {
  try {
    const uid = req._id

    const usuarioDB = await Usuario.findById(uid)

    if (usuarioDB == null) {
      const responseData: IResponseData = { data: { msg: 'Usuario No existe' }, status: 404, ok: false, res }
      return await sendInformation({ dataRecived: responseData })
    }

    if (usuarioDB.role !== 'ADMIN_ROLE') {
      const responseData: IResponseData = { data: { msg: 'No tiene privilegios para esto.' }, status: 403, ok: false, res }
      return await sendInformation({ dataRecived: responseData })
    }

    next()
  } catch (error) {
    const responseData: any = { data: { msg: 'Hable con el administrador' }, status: 500, error, ok: false, res }
    return await sendInformation({ dataRecived: responseData })
  }
}

const validarAdminRoleOrMe = async (req: CustomRequest, res: Response, next: NextFunction): Promise<any> => {
  try {
    const uid = req._id
    const id = req.params.id

    const usuarioDB = await Usuario.findById(uid)

    if (usuarioDB == null) {
      const responseData: IResponseData = { data: { msg: 'Usuario No existe' }, status: 404, ok: false, res }
      return await sendInformation({ dataRecived: responseData })
    }

    if (usuarioDB.role === 'ADMIN_ROLE' || uid === id) {
      next()
    } else {
      const responseData: IResponseData = { data: { msg: 'No tiene privilegios para esto.' }, status: 403, ok: false, res }
      return await sendInformation({ dataRecived: responseData })
    }
  } catch (error) {
    const responseData: any = { data: { msg: 'Hable con el administrador' }, status: 500, error, ok: false, res }
    return await sendInformation({ dataRecived: responseData })
  }
}

export { validateJWToken, validarAdminRole, validarAdminRoleOrMe }
