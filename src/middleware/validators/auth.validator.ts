import { check } from 'express-validator'
import { validateResults } from './useValidator'
import { Request, Response, NextFunction } from 'express'

const validatorLogin = [
  check('password', 'La contraseña es obligatoria.').notEmpty(),
  check('email', 'El correo electrónico es obligatorio.').isEmail(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]
const validatorLoginGoogle = [
  check('token', 'El token de google es obligatorio.').notEmpty(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]

export { validatorLogin, validatorLoginGoogle }
