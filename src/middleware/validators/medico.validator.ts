import { check } from 'express-validator'
import { validateResults } from './useValidator'
import { Request, Response, NextFunction } from 'express'

const validatorCrearMedico = [
  check('nombre', 'El nombre del medico es obligatorio.').notEmpty(),
  check('hospital', 'El hospital id debe ser valido').isMongoId(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]

const validatorActualizarMedico = [
  check('nombre', 'El nombre del medico es obligatorio.').notEmpty(),
  check('hospital', 'El hospital id debe ser valido').isMongoId(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]

export { validatorCrearMedico, validatorActualizarMedico }
