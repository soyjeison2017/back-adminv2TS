import { check } from 'express-validator'
import { validateResults } from './useValidator'
import { Request, Response, NextFunction } from 'express'

const validatorCrearUsuario = [
  check('nombre', 'El nombre es obligatorio.').notEmpty(),
  check('password', 'La contraseña es obligatoria.').notEmpty(),
  check('email', 'El correo electrónico es obligatorio.').isEmail(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]

const validatorActualizarUsuario = [
  check('nombre', 'El nombre es obligatorio.').notEmpty(),
  check('email', 'El correo electrónico es obligatorio.').isEmail(),
  check('role', 'El role es obligatorio.').notEmpty(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]

export { validatorCrearUsuario, validatorActualizarUsuario }
