import { RequestHandler } from 'express'
import { validationResult } from 'express-validator'
import { IResponseData } from '../../interfaces/ResponseData'

import { sendInformation } from '../../utils/handleInformation'

const validateResults: RequestHandler = (req, res, next) => {
  try {
    validationResult(req).throw()
    return next() // TODO: Continue
  } catch (err: any) {
    const sendValidate: IResponseData = {
      data: err.array(),
      ok: false,
      res,
      status: 400
    }
    sendInformation({ dataRecived: sendValidate }).catch(err => console.log('error', err))
  }
}
export { validateResults }
