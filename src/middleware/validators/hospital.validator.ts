import { check } from 'express-validator'
import { validateResults } from './useValidator'
import { Request, Response, NextFunction } from 'express'

const validatorCrearHospital = [
  check('nombre', 'El nombre es obligatorio.').notEmpty(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]
const validatorActualizarHospital = [
  check('nombre', 'El nombre es obligatorio.').notEmpty(),
  (req: Request, res: Response, next: NextFunction) => {
    return validateResults(req, res, next)
  }
]

export { validatorCrearHospital, validatorActualizarHospital }
