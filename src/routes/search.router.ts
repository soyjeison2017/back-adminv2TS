import { Router, Express, RequestHandler } from 'express'
import { validateJWToken } from '../middleware/jwt/validateJWT'
import { SearchController } from '../controllers/search.controller'
const router = Router()
const Search = (app: Express): void => {
  router
    .get('/:searchtodo', validateJWToken as RequestHandler, SearchController.getAllTotal as RequestHandler)
    .get('/coleccion/:tabla/:searchtodo', validateJWToken as RequestHandler, SearchController.getDocumentosColeccion as RequestHandler)

  app.use('/api/todo/', router)
}

export { Search }
