import { Router, Express, RequestHandler } from 'express'
import { validateJWToken } from '../middleware/jwt/validateJWT'
import { UploadController } from '../controllers/upload.controller'

import fileUpload from 'express-fileupload'
const router = Router()

const Uploads = (app: Express): void => {
  app.use(fileUpload())
  router
    .put('/:tipo/:id', validateJWToken as RequestHandler, UploadController.uploadFile as RequestHandler)
    .get('/:tipo/:foto', UploadController.getImage as RequestHandler)

  app.use('/api/upload/', router)
}

export { Uploads }
