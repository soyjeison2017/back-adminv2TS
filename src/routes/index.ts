import { User } from './user.router'
import { Hospital } from './hospital.router'
import { Medico } from './medico.router'
import { Auth } from './auth.router'
import { Express } from 'express'
import { Search } from './search.router'
import { Uploads } from './uploads.router'

const routesDefine = (app: Express): void => {
  Auth(app)
  User(app)
  Hospital(app)
  Medico(app)
  Search(app)
  Uploads(app)
}

export { routesDefine }
