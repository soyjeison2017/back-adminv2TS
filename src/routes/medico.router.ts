import { Router, Express, RequestHandler } from 'express'
import { MedicoController } from '../controllers/medico.controller'
import { validateJWToken } from '../middleware/jwt/validateJWT'
import { validatorActualizarMedico, validatorCrearMedico } from '../middleware/validators/medico.validator'
const router = Router()

const Medico = (app: Express): void => {
  router
    .get('/', MedicoController.getAllMedicos as RequestHandler)
    .get('/:id', validateJWToken as RequestHandler, MedicoController.getOneMedico as RequestHandler)
    .post('/', validateJWToken as RequestHandler, validatorCrearMedico, MedicoController.createMedico as RequestHandler)
    .put('/:id', validateJWToken as RequestHandler, validatorActualizarMedico, MedicoController.updateMedico as RequestHandler)
    .delete('/:id', validateJWToken as RequestHandler, MedicoController.deleteMedico as RequestHandler)

  app.use('/api/medico', router)
}

export { Medico }
