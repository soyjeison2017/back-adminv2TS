import { Router, Express, RequestHandler } from 'express'
import { UserController } from '../controllers/user.controller'
import { validatorCrearUsuario, validatorActualizarUsuario } from '../middleware/validators/user.validator'
import { validarAdminRole, validarAdminRoleOrMe, validateJWToken } from '../middleware/jwt/validateJWT'
const router = Router()
const User = (app: Express): void => {
  router
    .get('/', validateJWToken as RequestHandler, UserController.getAllUsers as RequestHandler)
    .post('/', validatorCrearUsuario, UserController.createUser as RequestHandler)
    .put('/:id', validateJWToken as RequestHandler, validarAdminRoleOrMe as RequestHandler, validatorActualizarUsuario, UserController.updateUser as RequestHandler)
    .delete('/:id', validateJWToken as RequestHandler, validarAdminRole as RequestHandler, UserController.deleteUser as RequestHandler)

  app.use('/api/users', router)
}

export { User }
