import { Router, Express, RequestHandler } from 'express'
import { HospitalController } from '../controllers/hospital.controller'
import { validateJWToken } from '../middleware/jwt/validateJWT'
import { validatorActualizarHospital, validatorCrearHospital } from '../middleware/validators/hospital.validator'
const router = Router()

const Hospital = (app: Express): void => {
  router
    .get('/', HospitalController.getAllHospitals as RequestHandler)
    .post('/', validateJWToken as RequestHandler, validatorCrearHospital, HospitalController.createHospital as RequestHandler)
    .put('/:id', validateJWToken as RequestHandler, validatorActualizarHospital, HospitalController.updateHospital as RequestHandler)
    .delete('/:id', validateJWToken as RequestHandler, HospitalController.deleteHospital as RequestHandler)

  app.use('/api/hospital', router)
}

export { Hospital }
