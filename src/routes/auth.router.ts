import { Router, Express, RequestHandler } from 'express'
import { AuthController } from '../controllers/auth.controller'
import { validatorLogin } from '../middleware/validators/auth.validator'
import { validateJWToken } from '../middleware/jwt/validateJWT'
const router = Router()
const Auth = (app: Express): void => {
  router
    .post('/', validatorLogin, AuthController.login as RequestHandler)
    .post('/google', AuthController.Logingoogle as RequestHandler)
    .get('/renew', validateJWToken as RequestHandler, AuthController.renewToken as RequestHandler)

  app.use('/api/login', router)
}

export { Auth }
