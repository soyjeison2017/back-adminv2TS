import mongoose from 'mongoose'
import 'dotenv/config'

export const dbConnection = async (): Promise<void> => {
  const DB_URI: string | undefined = process.env.DB_URI

  if (DB_URI === undefined) {
    throw new Error('DB_URI no esta definida en las variables de entorno.')
  }

  try {
    await mongoose.connect(DB_URI)
    console.log('*** CONEXIÓN DE BASE DE DATOS EXITOSA ***')
  } catch (error) {
    throw new Error('*** ERROR DE CONEXIÓN EN BASE DE DATOS***')
  }
}
