import { Response } from 'express'
export interface IResponseData {
  ok: boolean
  data: any
  status: number
  res: Response
  isFile?: boolean
  error?: any
}
