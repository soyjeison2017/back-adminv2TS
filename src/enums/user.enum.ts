enum Roles {
  user = 'USER_ROLE',
  admin = 'ADMIN_ROLE',
}

export { Roles }
