const menuExportFront = (role: string = 'USER_ROLE'): {} => {
  const menu: any[] = [
    {
      title: 'Principal',
      icon: 'mdi mdi-gauge',
      submenu: [
        { title: 'ProgressBar', url: 'progress' },
        { title: 'Gráficas', url: 'grafica1' },
        { title: 'Promesas', url: 'promises' },
        { title: 'Main', url: '/' },
        { title: 'Rxjs', url: 'rxjs' }
      ]
    },
    {
      title: 'Mantenimientos',
      icon: 'mdi mdi-folder-lock-open',
      submenu: [
        { title: 'Hospitales', url: 'hospitales' },
        { title: 'Medicos', url: 'medicos' }
      ]
    }
  ]

  if (role === 'ADMIN_ROLE') {
    menu[1].submenu.unshift({ title: 'Usuarios', url: 'usuarios' })
  }

  return menu
}

export { menuExportFront }
