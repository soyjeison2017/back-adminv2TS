import { Response } from 'express'
import { IResponseData } from '../interfaces/ResponseData'

import { sendMessageError } from '../bot/telegram'

const sendInformation = async ({ dataRecived }: { dataRecived: IResponseData }): Promise<Response<any> | undefined> => {
  const { data, ok, res, status, error, isFile } = dataRecived

  // console.log('🔴🔴🔴🔴🔴🔴 --> ', dataRecived)

  if (status >= 500) {
    await sendMessageError(error as string)
    return res.status(status ?? 500).send({ ok, data })
  } else if (ok && isFile !== true) {
    return res.status(status ?? 201).send({ ok, data })
  } else if (ok && isFile === true) {
    return res.status(status ?? 201).sendFile(data) as any
  } else {
    return res.status(status ?? 403).send({ ok, data })
  }
}

export { sendInformation }
