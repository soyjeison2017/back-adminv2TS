import { OAuth2Client } from 'google-auth-library'
import dotenv from 'dotenv'

dotenv.config({ path: './src/enviroments/.env' })
const client = new OAuth2Client(process.env.GOOGLE_SECRET)

async function googleVerify (token: string): Promise<any> {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.GOOGLE_ID // Specify the CLIENT_ID of the app that accesses the backend
    // Or, if multiple clients access the backend:
    // [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  })
  const payload = ticket.getPayload()
  //   console.log('PAYLOAD GOOGLE', payload)
  return payload
}

export { googleVerify }
