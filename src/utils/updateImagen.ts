import Usuario from '../models/user.model'
import Medico from '../models/medico.model'
import Hospital from '../models/hospital.model'

import path from 'path'
import fs from 'fs'

const deleteImg = (path: string): void => {
  // si existe la imagen
  const isDirectory = fs.statSync(path).isDirectory()

  if (!isDirectory) {
    if (fs.existsSync(path)) {
      // borra la imagen anterior
      fs.unlinkSync(path)
    }
  }
}

const actualizarImagen = async (tipo: any, id: any, nombreArchivo: any): Promise<any> => {
  console.log({ tipo, id, nombreArchivo })
  switch (tipo) {
    case 'medicos': {
      const medico = await Medico.findById(id)
      if (medico == null) {
        console.log('No es un médico por id')
        return false
      }
      // aquí esta la imagen antigua
      const img = medico?.img ?? ''
      const MEDIA_PATH = path.join(__dirname, `../uploads/medicos/${img}`)
      deleteImg(MEDIA_PATH)

      medico.img = nombreArchivo
      await medico.save()
      return true
    }

    case 'hospitales': {
      const hospital = await Hospital.findById(id)
      if (hospital == null) {
        console.log('No es un hospital por id')
        return false
      }
      // aquí esta la imagen antigua
      const img = hospital?.img ?? ''

      const MEDIA_PATH_HOSPITAL = path.join(__dirname, `../uploads/hospitales/${img}`)
      deleteImg(MEDIA_PATH_HOSPITAL)

      hospital.img = nombreArchivo
      await hospital.save()
      return true
    }

    case 'usuarios': {
      const usuario = await Usuario.findById(id)
      if (usuario == null) {
        console.log('No es un hospital por id')
        return false
      }
      // aquí esta la imagen antigua
      const img = usuario?.img ?? ''
      console.log(img)
      const MEDIA_PATH_USUARIO = path.join(__dirname, `../uploads/usuarios/${img}`)
      deleteImg(MEDIA_PATH_USUARIO)

      usuario.img = nombreArchivo
      await usuario.save()
      return true
    }
  }
}

export { actualizarImagen }
