import * as JWT from 'jsonwebtoken'
import { IJWT } from '../interfaces/JWT.interface'

const generateJWT = async (user: any): Promise<string> => {
  const payload: IJWT = {
    _id: user?.id
  }

  const firma = await JWT.sign(payload, process.env.JWT_TOKEN as string, { expiresIn: '24h' })
  return firma
}

export { generateJWT }
