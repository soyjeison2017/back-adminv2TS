import { Telegraf } from 'telegraf'
import dotenv from 'dotenv'

dotenv.config({ path: './src/enviroments/.env' })

const token: string = process.env.CHATBOT_TOKEN as string
const chatId: string = process.env.CHAT_ID as string

const getChatId = (): void => {
  const bot = new Telegraf(token)
  bot.start(async (ctx) => await ctx.reply('Welcome'))
  bot.hears('id', async (ctx) => {
    await ctx.reply(`ID DEL CHAT ${ctx.update.message.chat.id}`)
  })

  bot.launch().catch((err) => console.error(err))
}

const sendMessageError = async (messageToSend: string): Promise<void> => {
  try {
    const response = await fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chatId}&text=${messageToSend}`)

    await response.json()
  } catch (error) {
    console.log('🔴🔴🔴🔴🔴🔴 ERROR AL ENVIAR UN MSJ A TELEGRAM 🔴🔴🔴🔴🔴🔴', error)
  }
}

export {
  getChatId,
  sendMessageError
}
