import express from 'express'
import dotenv from 'dotenv'
import cors from 'cors'

import { dbConnection } from './src/database/config'
import { routesDefine } from './src/routes'

dotenv.config({ path: './src/enviroments/.env' })

const app = express()
const port = process.env.PORT ?? 4000
app.use(cors())

app.use(express.static('./src/public'))

app.use(express.json())

// Rutas
routesDefine(app)

app.listen(port, () => {
  const msg: string = `Servidor corriendo en http://localhost:${port}`
  console.log(msg)
})

void dbConnection()
